###########

# From here: https://css-tricks.com/learning-gutenberg-6-setting-up-a-custom-webpack-config/

###########

cd to plugin folder

npm init

npm install webpack --save-dev

npm install webpack-cli --save-dev

create webpack.config.js in root folder, where package.json is etc. See webpack.config.js file for configuration

# The below commands can obviously be condensed into one command but had them like this to test if any threw errors on install

npm install @babel/core --save-dev
npm install @babel/preset-env --save-dev
npm install babel-loader --save-dev
npm install babel-plugin-transform-react-jsx --save-dev
(and maybe npm install babel-plugin-add-module-exports --save-dev although i think this only applies to Babel 6)

create .babelrc file in root folder. See .babelrc file for configuration

# The below is from the tutorial and can be done differently depending on how you want to set up your files and folders. Just need to make sure that entries in the webpack.config.js reflect your differing setup.

Go into your plugin directory and create a folder called blocks and, within that, create two folders: one called src and one called dist.

Inside the src folder, create the following files. We’ve added the paths, too, so you put them in the right place:

    block.js

Then to make sure everything works run:
npx webpack
