<?php
/**
 * Plugin Name: ND Block Width
 * Description: Plugin to allow the changing of the width of the columns block.
 * Author: ND
 * Version: 1.0.0
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: add-block-width
 * Domain Path: /languages/
 *
 * @package add-block-width
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

add_action( 'enqueue_block_editor_assets', 'nd_add_block_width' );

function nd_add_block_width() {
    // Enqueue our script
    wp_enqueue_script(
        'nd-add-block-width',
        esc_url( plugins_url( 'blocks/dist/blocks.build.js', __FILE__ ) ),
        array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ),
        '1.0.0',
        true // Enqueue the script in the footer.
    );
}