/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./blocks/src/block.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./blocks/src/block.js":
/*!*****************************!*\
  !*** ./blocks/src/block.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Based on: https://www.liip.ch/en/blog/how-to-extend-existing-gutenberg-blocks-in-wordpress
// import assign from "lodash/assign";
var addFilter = wp.hooks.addFilter;
var __ = wp.i18n.__;
var createHigherOrderComponent = wp.compose.createHigherOrderComponent;
var Fragment = wp.element.Fragment;
var InspectorControls = wp.blockEditor.InspectorControls;
var _wp$components = wp.components,
    PanelBody = _wp$components.PanelBody,
    RangeControl = _wp$components.RangeControl;
var enableWidthOnBlocks = ["core/columns"];
/**
 * Add width attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */

var addWidthAttribute = function addWidthAttribute(settings, name) {
  // Do nothing if it's another block than our defined ones.
  if (!enableWidthOnBlocks.includes(name)) {
    return settings;
  } // Use Lodash's assign to gracefully handle if attributes are undefined
  // settings.attributes = assign(settings.attributes, {
  //   width: {
  //     type: "number",
  //     default: 100
  //   }
  // });


  settings.attributes = _objectSpread(_objectSpread({}, settings.attributes), {}, {
    width: {
      type: "number",
      "default": 100
    }
  });
  return settings;
};

addFilter("blocks.registerBlockType", "nd-block-width/attribute/width", addWidthAttribute);
/**
 * Create HOC to add width control to inspector controls of block.
 */

var widthControl = createHigherOrderComponent(function (BlockEdit) {
  return function (props) {
    // Do nothing if it's another block than our defined ones.
    if (!enableWidthOnBlocks.includes(props.name)) {
      return wp.element.createElement(BlockEdit, props);
    }

    var width = props.attributes.width;
    return wp.element.createElement(Fragment, null, wp.element.createElement(BlockEdit, props), wp.element.createElement(InspectorControls, null, wp.element.createElement(PanelBody, {
      title: __("Width Control"),
      initialOpen: true
    }, wp.element.createElement(RangeControl, {
      label: "Width",
      value: width,
      onChange: function onChange(selectedWidthValue) {
        props.setAttributes({
          width: selectedWidthValue
        });
      },
      min: 1,
      max: 100
    }))));
  };
}, "widthControl");
addFilter("editor.BlockEdit", "nd-block-width/width-control", widthControl);
/**
 * Alter the outer elements of the columns block, leaving the inner columns untouched.
 *
 * @param {object} element The element to edit. i.e. The core/columns element
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified columns element.
 */

var alterBlock = function alterBlock(element, blockType, attributes) {
  // Do nothing if it's another block than our defined ones.
  if (!enableWidthOnBlocks.includes(blockType.name) || attributes.width === 100) {
    return element;
  }

  return wp.element.createElement("div", {
    className: "".concat(element.props.className),
    style: {
      justifyContent: "center"
    }
  }, wp.element.createElement("div", {
    style: {
      width: "".concat(attributes.width, "%"),
      display: "flex"
    }
  }, element.props.children));
};

addFilter("blocks.getSaveElement", "nd-block-width/alter-block", alterBlock); // No longer required as I needed to do more than just add style to the outer element. I needed to add an extra div too.
// That functionality is now catered for in alterBlock

/**
 * Add width style attribute to save element of block.
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
// const addWidthExtraProps = (saveElementProps, blockType, attributes) => {
//   // Do nothing if it's another block than our defined ones.
//   if (!enableWidthOnBlocks.includes(blockType.name)) {
//     return saveElementProps;
//   }
//   // Different approaches to creating the object
//   // Use Lodash's assign to gracefully handle if attributes are undefined
//   // assign(saveElementProps, { style: { width: `${attributes.width}%` } });
//   // Object.assign(saveElementProps, { style: { width: `${attributes.width}%` } });
//   // const newProps = { ...saveElementProps, style: { width: `${attributes.width}%` } };
//   saveElementProps = { ...saveElementProps, style: { width: `${attributes.width}%` } };
//   return saveElementProps;
// };
//addFilter("blocks.getSaveContent.extraProps", "nd-block-width/get-save-content/extra-props", addWidthExtraProps);

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYmxvY2tzL3NyYy9ibG9jay5qcyJdLCJuYW1lcyI6WyJhZGRGaWx0ZXIiLCJ3cCIsImhvb2tzIiwiX18iLCJpMThuIiwiY3JlYXRlSGlnaGVyT3JkZXJDb21wb25lbnQiLCJjb21wb3NlIiwiRnJhZ21lbnQiLCJlbGVtZW50IiwiSW5zcGVjdG9yQ29udHJvbHMiLCJibG9ja0VkaXRvciIsImNvbXBvbmVudHMiLCJQYW5lbEJvZHkiLCJSYW5nZUNvbnRyb2wiLCJlbmFibGVXaWR0aE9uQmxvY2tzIiwiYWRkV2lkdGhBdHRyaWJ1dGUiLCJzZXR0aW5ncyIsIm5hbWUiLCJpbmNsdWRlcyIsImF0dHJpYnV0ZXMiLCJ3aWR0aCIsInR5cGUiLCJ3aWR0aENvbnRyb2wiLCJCbG9ja0VkaXQiLCJwcm9wcyIsInNlbGVjdGVkV2lkdGhWYWx1ZSIsInNldEF0dHJpYnV0ZXMiLCJhbHRlckJsb2NrIiwiYmxvY2tUeXBlIiwiY2xhc3NOYW1lIiwianVzdGlmeUNvbnRlbnQiLCJkaXNwbGF5IiwiY2hpbGRyZW4iXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUVBO0lBQ1FBLFMsR0FBY0MsRUFBRSxDQUFDQyxLLENBQWpCRixTO0lBQ0FHLEUsR0FBT0YsRUFBRSxDQUFDRyxJLENBQVZELEU7SUFDQUUsMEIsR0FBK0JKLEVBQUUsQ0FBQ0ssTyxDQUFsQ0QsMEI7SUFDQUUsUSxHQUFhTixFQUFFLENBQUNPLE8sQ0FBaEJELFE7SUFDQUUsaUIsR0FBc0JSLEVBQUUsQ0FBQ1MsVyxDQUF6QkQsaUI7cUJBQzRCUixFQUFFLENBQUNVLFU7SUFBL0JDLFMsa0JBQUFBLFM7SUFBV0MsWSxrQkFBQUEsWTtBQUVuQixJQUFNQyxtQkFBbUIsR0FBRyxDQUFDLGNBQUQsQ0FBNUI7QUFFQTs7Ozs7Ozs7O0FBUUEsSUFBTUMsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFDQyxRQUFELEVBQVdDLElBQVgsRUFBb0I7QUFDNUM7QUFDQSxNQUFJLENBQUNILG1CQUFtQixDQUFDSSxRQUFwQixDQUE2QkQsSUFBN0IsQ0FBTCxFQUF5QztBQUN2QyxXQUFPRCxRQUFQO0FBQ0QsR0FKMkMsQ0FNNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBQSxVQUFRLENBQUNHLFVBQVQsbUNBQ0tILFFBQVEsQ0FBQ0csVUFEZDtBQUVFQyxTQUFLLEVBQUU7QUFDTEMsVUFBSSxFQUFFLFFBREQ7QUFFTCxpQkFBUztBQUZKO0FBRlQ7QUFRQSxTQUFPTCxRQUFQO0FBQ0QsQ0F2QkQ7O0FBeUJBaEIsU0FBUyxDQUFDLDBCQUFELEVBQTZCLGdDQUE3QixFQUErRGUsaUJBQS9ELENBQVQ7QUFFQTs7OztBQUdBLElBQU1PLFlBQVksR0FBR2pCLDBCQUEwQixDQUFDLFVBQUFrQixTQUFTLEVBQUk7QUFDM0QsU0FBTyxVQUFBQyxLQUFLLEVBQUk7QUFDZDtBQUNBLFFBQUksQ0FBQ1YsbUJBQW1CLENBQUNJLFFBQXBCLENBQTZCTSxLQUFLLENBQUNQLElBQW5DLENBQUwsRUFBK0M7QUFDN0MsYUFBTyx5QkFBQyxTQUFELEVBQWVPLEtBQWYsQ0FBUDtBQUNEOztBQUphLFFBTU5KLEtBTk0sR0FNSUksS0FBSyxDQUFDTCxVQU5WLENBTU5DLEtBTk07QUFRZCxXQUNFLHlCQUFDLFFBQUQsUUFDRSx5QkFBQyxTQUFELEVBQWVJLEtBQWYsQ0FERixFQUVFLHlCQUFDLGlCQUFELFFBQ0UseUJBQUMsU0FBRDtBQUFXLFdBQUssRUFBRXJCLEVBQUUsQ0FBQyxlQUFELENBQXBCO0FBQXVDLGlCQUFXLEVBQUU7QUFBcEQsT0FDRSx5QkFBQyxZQUFEO0FBQ0UsV0FBSyxFQUFDLE9BRFI7QUFFRSxXQUFLLEVBQUVpQixLQUZUO0FBR0UsY0FBUSxFQUFFLGtCQUFBSyxrQkFBa0IsRUFBSTtBQUM5QkQsYUFBSyxDQUFDRSxhQUFOLENBQW9CO0FBQ2xCTixlQUFLLEVBQUVLO0FBRFcsU0FBcEI7QUFHRCxPQVBIO0FBUUUsU0FBRyxFQUFFLENBUlA7QUFTRSxTQUFHLEVBQUU7QUFUUCxNQURGLENBREYsQ0FGRixDQURGO0FBb0JELEdBNUJEO0FBNkJELENBOUI4QyxFQThCNUMsY0E5QjRDLENBQS9DO0FBZ0NBekIsU0FBUyxDQUFDLGtCQUFELEVBQXFCLDhCQUFyQixFQUFxRHNCLFlBQXJELENBQVQ7QUFFQTs7Ozs7Ozs7OztBQVVBLElBQU1LLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNuQixPQUFELEVBQVVvQixTQUFWLEVBQXFCVCxVQUFyQixFQUFvQztBQUNyRDtBQUNBLE1BQUksQ0FBQ0wsbUJBQW1CLENBQUNJLFFBQXBCLENBQTZCVSxTQUFTLENBQUNYLElBQXZDLENBQUQsSUFBaURFLFVBQVUsQ0FBQ0MsS0FBWCxLQUFxQixHQUExRSxFQUErRTtBQUM3RSxXQUFPWixPQUFQO0FBQ0Q7O0FBRUQsU0FDRTtBQUFLLGFBQVMsWUFBS0EsT0FBTyxDQUFDZ0IsS0FBUixDQUFjSyxTQUFuQixDQUFkO0FBQThDLFNBQUssRUFBRTtBQUFFQyxvQkFBYyxFQUFFO0FBQWxCO0FBQXJELEtBQ0U7QUFBSyxTQUFLLEVBQUU7QUFBRVYsV0FBSyxZQUFLRCxVQUFVLENBQUNDLEtBQWhCLE1BQVA7QUFBaUNXLGFBQU8sRUFBRTtBQUExQztBQUFaLEtBQWlFdkIsT0FBTyxDQUFDZ0IsS0FBUixDQUFjUSxRQUEvRSxDQURGLENBREY7QUFLRCxDQVhEOztBQWFBaEMsU0FBUyxDQUFDLHVCQUFELEVBQTBCLDRCQUExQixFQUF3RDJCLFVBQXhELENBQVQsQyxDQUVBO0FBQ0E7O0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUNBO0FBRUEsbUgiLCJmaWxlIjoiYmxvY2tzLmJ1aWxkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9ibG9ja3Mvc3JjL2Jsb2NrLmpzXCIpO1xuIiwiLy8gQmFzZWQgb246IGh0dHBzOi8vd3d3LmxpaXAuY2gvZW4vYmxvZy9ob3ctdG8tZXh0ZW5kLWV4aXN0aW5nLWd1dGVuYmVyZy1ibG9ja3MtaW4td29yZHByZXNzXHJcblxyXG4vLyBpbXBvcnQgYXNzaWduIGZyb20gXCJsb2Rhc2gvYXNzaWduXCI7XHJcbmNvbnN0IHsgYWRkRmlsdGVyIH0gPSB3cC5ob29rcztcclxuY29uc3QgeyBfXyB9ID0gd3AuaTE4bjtcclxuY29uc3QgeyBjcmVhdGVIaWdoZXJPcmRlckNvbXBvbmVudCB9ID0gd3AuY29tcG9zZTtcclxuY29uc3QgeyBGcmFnbWVudCB9ID0gd3AuZWxlbWVudDtcclxuY29uc3QgeyBJbnNwZWN0b3JDb250cm9scyB9ID0gd3AuYmxvY2tFZGl0b3I7XHJcbmNvbnN0IHsgUGFuZWxCb2R5LCBSYW5nZUNvbnRyb2wgfSA9IHdwLmNvbXBvbmVudHM7XHJcblxyXG5jb25zdCBlbmFibGVXaWR0aE9uQmxvY2tzID0gW1wiY29yZS9jb2x1bW5zXCJdO1xyXG5cclxuLyoqXHJcbiAqIEFkZCB3aWR0aCBhdHRyaWJ1dGUgdG8gYmxvY2suXHJcbiAqXHJcbiAqIEBwYXJhbSB7b2JqZWN0fSBzZXR0aW5ncyBDdXJyZW50IGJsb2NrIHNldHRpbmdzLlxyXG4gKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBOYW1lIG9mIGJsb2NrLlxyXG4gKlxyXG4gKiBAcmV0dXJucyB7b2JqZWN0fSBNb2RpZmllZCBibG9jayBzZXR0aW5ncy5cclxuICovXHJcbmNvbnN0IGFkZFdpZHRoQXR0cmlidXRlID0gKHNldHRpbmdzLCBuYW1lKSA9PiB7XHJcbiAgLy8gRG8gbm90aGluZyBpZiBpdCdzIGFub3RoZXIgYmxvY2sgdGhhbiBvdXIgZGVmaW5lZCBvbmVzLlxyXG4gIGlmICghZW5hYmxlV2lkdGhPbkJsb2Nrcy5pbmNsdWRlcyhuYW1lKSkge1xyXG4gICAgcmV0dXJuIHNldHRpbmdzO1xyXG4gIH1cclxuXHJcbiAgLy8gVXNlIExvZGFzaCdzIGFzc2lnbiB0byBncmFjZWZ1bGx5IGhhbmRsZSBpZiBhdHRyaWJ1dGVzIGFyZSB1bmRlZmluZWRcclxuICAvLyBzZXR0aW5ncy5hdHRyaWJ1dGVzID0gYXNzaWduKHNldHRpbmdzLmF0dHJpYnV0ZXMsIHtcclxuICAvLyAgIHdpZHRoOiB7XHJcbiAgLy8gICAgIHR5cGU6IFwibnVtYmVyXCIsXHJcbiAgLy8gICAgIGRlZmF1bHQ6IDEwMFxyXG4gIC8vICAgfVxyXG4gIC8vIH0pO1xyXG5cclxuICBzZXR0aW5ncy5hdHRyaWJ1dGVzID0ge1xyXG4gICAgLi4uc2V0dGluZ3MuYXR0cmlidXRlcyxcclxuICAgIHdpZHRoOiB7XHJcbiAgICAgIHR5cGU6IFwibnVtYmVyXCIsXHJcbiAgICAgIGRlZmF1bHQ6IDEwMFxyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHJldHVybiBzZXR0aW5ncztcclxufTtcclxuXHJcbmFkZEZpbHRlcihcImJsb2Nrcy5yZWdpc3RlckJsb2NrVHlwZVwiLCBcIm5kLWJsb2NrLXdpZHRoL2F0dHJpYnV0ZS93aWR0aFwiLCBhZGRXaWR0aEF0dHJpYnV0ZSk7XHJcblxyXG4vKipcclxuICogQ3JlYXRlIEhPQyB0byBhZGQgd2lkdGggY29udHJvbCB0byBpbnNwZWN0b3IgY29udHJvbHMgb2YgYmxvY2suXHJcbiAqL1xyXG5jb25zdCB3aWR0aENvbnRyb2wgPSBjcmVhdGVIaWdoZXJPcmRlckNvbXBvbmVudChCbG9ja0VkaXQgPT4ge1xyXG4gIHJldHVybiBwcm9wcyA9PiB7XHJcbiAgICAvLyBEbyBub3RoaW5nIGlmIGl0J3MgYW5vdGhlciBibG9jayB0aGFuIG91ciBkZWZpbmVkIG9uZXMuXHJcbiAgICBpZiAoIWVuYWJsZVdpZHRoT25CbG9ja3MuaW5jbHVkZXMocHJvcHMubmFtZSkpIHtcclxuICAgICAgcmV0dXJuIDxCbG9ja0VkaXQgey4uLnByb3BzfSAvPjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCB7IHdpZHRoIH0gPSBwcm9wcy5hdHRyaWJ1dGVzO1xyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxGcmFnbWVudD5cclxuICAgICAgICA8QmxvY2tFZGl0IHsuLi5wcm9wc30gLz5cclxuICAgICAgICA8SW5zcGVjdG9yQ29udHJvbHM+XHJcbiAgICAgICAgICA8UGFuZWxCb2R5IHRpdGxlPXtfXyhcIldpZHRoIENvbnRyb2xcIil9IGluaXRpYWxPcGVuPXt0cnVlfT5cclxuICAgICAgICAgICAgPFJhbmdlQ29udHJvbFxyXG4gICAgICAgICAgICAgIGxhYmVsPVwiV2lkdGhcIlxyXG4gICAgICAgICAgICAgIHZhbHVlPXt3aWR0aH1cclxuICAgICAgICAgICAgICBvbkNoYW5nZT17c2VsZWN0ZWRXaWR0aFZhbHVlID0+IHtcclxuICAgICAgICAgICAgICAgIHByb3BzLnNldEF0dHJpYnV0ZXMoe1xyXG4gICAgICAgICAgICAgICAgICB3aWR0aDogc2VsZWN0ZWRXaWR0aFZhbHVlXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgIG1pbj17MX1cclxuICAgICAgICAgICAgICBtYXg9ezEwMH1cclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgIDwvUGFuZWxCb2R5PlxyXG4gICAgICAgIDwvSW5zcGVjdG9yQ29udHJvbHM+XHJcbiAgICAgIDwvRnJhZ21lbnQ+XHJcbiAgICApO1xyXG4gIH07XHJcbn0sIFwid2lkdGhDb250cm9sXCIpO1xyXG5cclxuYWRkRmlsdGVyKFwiZWRpdG9yLkJsb2NrRWRpdFwiLCBcIm5kLWJsb2NrLXdpZHRoL3dpZHRoLWNvbnRyb2xcIiwgd2lkdGhDb250cm9sKTtcclxuXHJcbi8qKlxyXG4gKiBBbHRlciB0aGUgb3V0ZXIgZWxlbWVudHMgb2YgdGhlIGNvbHVtbnMgYmxvY2ssIGxlYXZpbmcgdGhlIGlubmVyIGNvbHVtbnMgdW50b3VjaGVkLlxyXG4gKlxyXG4gKiBAcGFyYW0ge29iamVjdH0gZWxlbWVudCBUaGUgZWxlbWVudCB0byBlZGl0LiBpLmUuIFRoZSBjb3JlL2NvbHVtbnMgZWxlbWVudFxyXG4gKiBAcGFyYW0ge09iamVjdH0gYmxvY2tUeXBlIEJsb2NrIHR5cGUgaW5mb3JtYXRpb24uXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSBhdHRyaWJ1dGVzIEF0dHJpYnV0ZXMgb2YgYmxvY2suXHJcbiAqXHJcbiAqIEByZXR1cm5zIHtvYmplY3R9IE1vZGlmaWVkIGNvbHVtbnMgZWxlbWVudC5cclxuICovXHJcblxyXG5jb25zdCBhbHRlckJsb2NrID0gKGVsZW1lbnQsIGJsb2NrVHlwZSwgYXR0cmlidXRlcykgPT4ge1xyXG4gIC8vIERvIG5vdGhpbmcgaWYgaXQncyBhbm90aGVyIGJsb2NrIHRoYW4gb3VyIGRlZmluZWQgb25lcy5cclxuICBpZiAoIWVuYWJsZVdpZHRoT25CbG9ja3MuaW5jbHVkZXMoYmxvY2tUeXBlLm5hbWUpIHx8IGF0dHJpYnV0ZXMud2lkdGggPT09IDEwMCkge1xyXG4gICAgcmV0dXJuIGVsZW1lbnQ7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPGRpdiBjbGFzc05hbWU9e2Ake2VsZW1lbnQucHJvcHMuY2xhc3NOYW1lfWB9IHN0eWxlPXt7IGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiIH19PlxyXG4gICAgICA8ZGl2IHN0eWxlPXt7IHdpZHRoOiBgJHthdHRyaWJ1dGVzLndpZHRofSVgLCBkaXNwbGF5OiBcImZsZXhcIiB9fT57ZWxlbWVudC5wcm9wcy5jaGlsZHJlbn08L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gICk7XHJcbn07XHJcblxyXG5hZGRGaWx0ZXIoXCJibG9ja3MuZ2V0U2F2ZUVsZW1lbnRcIiwgXCJuZC1ibG9jay13aWR0aC9hbHRlci1ibG9ja1wiLCBhbHRlckJsb2NrKTtcclxuXHJcbi8vIE5vIGxvbmdlciByZXF1aXJlZCBhcyBJIG5lZWRlZCB0byBkbyBtb3JlIHRoYW4ganVzdCBhZGQgc3R5bGUgdG8gdGhlIG91dGVyIGVsZW1lbnQuIEkgbmVlZGVkIHRvIGFkZCBhbiBleHRyYSBkaXYgdG9vLlxyXG4vLyBUaGF0IGZ1bmN0aW9uYWxpdHkgaXMgbm93IGNhdGVyZWQgZm9yIGluIGFsdGVyQmxvY2tcclxuLyoqXHJcbiAqIEFkZCB3aWR0aCBzdHlsZSBhdHRyaWJ1dGUgdG8gc2F2ZSBlbGVtZW50IG9mIGJsb2NrLlxyXG4gKlxyXG4gKiBAcGFyYW0ge29iamVjdH0gc2F2ZUVsZW1lbnRQcm9wcyBQcm9wcyBvZiBzYXZlIGVsZW1lbnQuXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSBibG9ja1R5cGUgQmxvY2sgdHlwZSBpbmZvcm1hdGlvbi5cclxuICogQHBhcmFtIHtPYmplY3R9IGF0dHJpYnV0ZXMgQXR0cmlidXRlcyBvZiBibG9jay5cclxuICpcclxuICogQHJldHVybnMge29iamVjdH0gTW9kaWZpZWQgcHJvcHMgb2Ygc2F2ZSBlbGVtZW50LlxyXG4gKi9cclxuLy8gY29uc3QgYWRkV2lkdGhFeHRyYVByb3BzID0gKHNhdmVFbGVtZW50UHJvcHMsIGJsb2NrVHlwZSwgYXR0cmlidXRlcykgPT4ge1xyXG4vLyAgIC8vIERvIG5vdGhpbmcgaWYgaXQncyBhbm90aGVyIGJsb2NrIHRoYW4gb3VyIGRlZmluZWQgb25lcy5cclxuLy8gICBpZiAoIWVuYWJsZVdpZHRoT25CbG9ja3MuaW5jbHVkZXMoYmxvY2tUeXBlLm5hbWUpKSB7XHJcbi8vICAgICByZXR1cm4gc2F2ZUVsZW1lbnRQcm9wcztcclxuLy8gICB9XHJcblxyXG4vLyAgIC8vIERpZmZlcmVudCBhcHByb2FjaGVzIHRvIGNyZWF0aW5nIHRoZSBvYmplY3RcclxuXHJcbi8vICAgLy8gVXNlIExvZGFzaCdzIGFzc2lnbiB0byBncmFjZWZ1bGx5IGhhbmRsZSBpZiBhdHRyaWJ1dGVzIGFyZSB1bmRlZmluZWRcclxuLy8gICAvLyBhc3NpZ24oc2F2ZUVsZW1lbnRQcm9wcywgeyBzdHlsZTogeyB3aWR0aDogYCR7YXR0cmlidXRlcy53aWR0aH0lYCB9IH0pO1xyXG5cclxuLy8gICAvLyBPYmplY3QuYXNzaWduKHNhdmVFbGVtZW50UHJvcHMsIHsgc3R5bGU6IHsgd2lkdGg6IGAke2F0dHJpYnV0ZXMud2lkdGh9JWAgfSB9KTtcclxuXHJcbi8vICAgLy8gY29uc3QgbmV3UHJvcHMgPSB7IC4uLnNhdmVFbGVtZW50UHJvcHMsIHN0eWxlOiB7IHdpZHRoOiBgJHthdHRyaWJ1dGVzLndpZHRofSVgIH0gfTtcclxuXHJcbi8vICAgc2F2ZUVsZW1lbnRQcm9wcyA9IHsgLi4uc2F2ZUVsZW1lbnRQcm9wcywgc3R5bGU6IHsgd2lkdGg6IGAke2F0dHJpYnV0ZXMud2lkdGh9JWAgfSB9O1xyXG5cclxuLy8gICByZXR1cm4gc2F2ZUVsZW1lbnRQcm9wcztcclxuLy8gfTtcclxuXHJcbi8vYWRkRmlsdGVyKFwiYmxvY2tzLmdldFNhdmVDb250ZW50LmV4dHJhUHJvcHNcIiwgXCJuZC1ibG9jay13aWR0aC9nZXQtc2F2ZS1jb250ZW50L2V4dHJhLXByb3BzXCIsIGFkZFdpZHRoRXh0cmFQcm9wcyk7XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=