// Based on: https://www.liip.ch/en/blog/how-to-extend-existing-gutenberg-blocks-in-wordpress

// import assign from "lodash/assign";
const { addFilter } = wp.hooks;
const { __ } = wp.i18n;
const { createHigherOrderComponent } = wp.compose;
const { Fragment } = wp.element;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, RangeControl } = wp.components;

const enableWidthOnBlocks = ["core/columns"];

/**
 * Add width attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */
const addWidthAttribute = (settings, name) => {
  // Do nothing if it's another block than our defined ones.
  if (!enableWidthOnBlocks.includes(name)) {
    return settings;
  }

  // Use Lodash's assign to gracefully handle if attributes are undefined
  // settings.attributes = assign(settings.attributes, {
  //   width: {
  //     type: "number",
  //     default: 100
  //   }
  // });

  settings.attributes = {
    ...settings.attributes,
    width: {
      type: "number",
      default: 100
    }
  };

  return settings;
};

addFilter("blocks.registerBlockType", "nd-block-width/attribute/width", addWidthAttribute);

/**
 * Create HOC to add width control to inspector controls of block.
 */
const widthControl = createHigherOrderComponent(BlockEdit => {
  return props => {
    // Do nothing if it's another block than our defined ones.
    if (!enableWidthOnBlocks.includes(props.name)) {
      return <BlockEdit {...props} />;
    }

    const { width } = props.attributes;

    return (
      <Fragment>
        <BlockEdit {...props} />
        <InspectorControls>
          <PanelBody title={__("Width Control")} initialOpen={true}>
            <RangeControl
              label="Width"
              value={width}
              onChange={selectedWidthValue => {
                props.setAttributes({
                  width: selectedWidthValue
                });
              }}
              min={1}
              max={100}
            />
          </PanelBody>
        </InspectorControls>
      </Fragment>
    );
  };
}, "widthControl");

addFilter("editor.BlockEdit", "nd-block-width/width-control", widthControl);

/**
 * Alter the outer elements of the columns block, leaving the inner columns untouched.
 *
 * @param {object} element The element to edit. i.e. The core/columns element
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified columns element.
 */

const alterBlock = (element, blockType, attributes) => {
  // Do nothing if it's another block than our defined ones.
  // Or if the block width is set to 100% (because adding extra HTML for that would be redundant)
  if (!enableWidthOnBlocks.includes(blockType.name) || attributes.width === 100) {
    return element;
  }

  return (
    <div className={`${element.props.className}`} style={{ justifyContent: "center" }}>
      <div style={{ width: `${attributes.width}%`, display: "flex" }}>{element.props.children}</div>
    </div>
  );
};

addFilter("blocks.getSaveElement", "nd-block-width/alter-block", alterBlock);

// No longer required as I needed to do more than just add style to the outer element. I needed to add an extra div too.
// That functionality is now catered for in alterBlock
/**
 * Add width style attribute to save element of block.
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
// const addWidthExtraProps = (saveElementProps, blockType, attributes) => {
//   // Do nothing if it's another block than our defined ones.
//   if (!enableWidthOnBlocks.includes(blockType.name)) {
//     return saveElementProps;
//   }

//   // Different approaches to creating the object

//   // Use Lodash's assign to gracefully handle if attributes are undefined
//   // assign(saveElementProps, { style: { width: `${attributes.width}%` } });

//   // Object.assign(saveElementProps, { style: { width: `${attributes.width}%` } });

//   // const newProps = { ...saveElementProps, style: { width: `${attributes.width}%` } };

//   saveElementProps = { ...saveElementProps, style: { width: `${attributes.width}%` } };

//   return saveElementProps;
// };

//addFilter("blocks.getSaveContent.extraProps", "nd-block-width/get-save-content/extra-props", addWidthExtraProps);
